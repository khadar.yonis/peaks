# Objectif: créer une API de notation d'élèves en Symfony 

# Prérequis
    - minimum PHP 7.2.5
    - symfony flex
    - npm 
    - mysql

# Technologies utilisés 
    - Frontend :
        - React : ^16.13.1
    - Backend : 
        - Symfony : 5.1.*
        - ApiPlatform : ^1.2 

# Installation 

    - cloner le dépôt du projet  
        git clone https://gitlab.com/khadar.yonis/peaks.git <donnez_un_nom>

    - cd <donnez_un_nom>

    - installer les dépendances backend 
        composer install

    - installer les dépendancesfrontend 
        npm install

    - créer la base de données (ecole)  
        php bin/console doctrine:database:create

        Attention : dans mon fichier de .env ligne 29 (à remplacer 1er 'root' par votre utilisateur mysql
        et le 2nd 'root' par votre mot de passe mysql et '8889' par votre port et 'ecole' par le nom que vous voulez
        donné à votre passe de données)
        DATABASE_URL=mysql://root:root@127.0.0.1:8889/ecole?serverVersion=5.7


    - lancer les migrations
        php bin/console doctrine:migrations:migrate

    - charger les data fixtures 
        php bin/console doctrine:fixtures:load —n

    - lancer le serveur backend 
        symfony server:start

    - lancer le serveur frontend  
        npm run dev
    
    Enfin, sur votre navigateur, ouvrez vore serveur local, pour moi c'est : https://localhost:8000 
    (si ce n'est pas votre cas vous pourriez changer le fichier de config dans assets/js/config.js)



# Fonctionnalités 
    L'application est sécurisée, pour pouvoir utiliser ses fonctionnalités, soit il faut créer un
    nouvel utilisateur via le formulaire d'inscription soit connecter avec les utilisateurs existants déjà dans 
    la base de donées (table user) avec "password" comme mot de passe par défaut.

    Dans la suite, on considère les utilisateurs comme étant les professeurs car c'est à eux de créer leur classes, 
    ses élèves, leur notes et les matières.

    Un professeur connecté a la possibilité de :
        -  voir la liste de ses classes et leur moyen général, ses élèves et leur moyen général, leurs notes et les matières
        -  ajouter une classe, un élève, une note et une matière
        -  modifier une classe, un élève, une note et une matière
        -  supprimer une classe, un élève, une note et une matière

# Bonus
    - sécurité avec authentification (jwt)
    - pagination (component)
    - système de cache
    - système de notification (react toast)
    - système de loader (react tableLoader)
    - un fichier de config pour les urls 









