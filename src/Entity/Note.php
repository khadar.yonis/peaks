<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 * @ApiResource(
 *  normalizationContext={"groups"={"notes_read"}},
 *  denormalizationContext={"disable_type_enforcement"=true}
 * )
 */
class Note
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"notes_read", "students_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="La note est obligatoire")
     * @Groups({"notes_read", "students_read"})
     * @Assert\Type(type="numeric", message="Le coefficient doit être un numérique")
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="notes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"notes_read"})
     * @Assert\NotBlank(message="L'élève est obligatoire")
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Subject::class, inversedBy="notes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"notes_read", "students_read"})
     * @Assert\NotBlank(message="La matière est obligatoire")
     */
    private $subject;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore($score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }
}
