<?php

namespace App\Entity;

use App\Repository\ClasseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClasseRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups"={"classes_read"}}
 * )
 */
class Classe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"classes_read", "students_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"classes_read", "students_read"})
     * @Assert\Length(min=3, minMessage="Le nom de la classe doit faire entre 3 et 255 caractères", max=255, maxMessage="Le nom de la classe doit faire entre 3 et 255 caractères")
     * @Assert\NotBlank(message="Le nom de classe est obligatoire")
     */
    private $name; 

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="classe", orphanRemoval=true)
     * @Groups({"classes_read"})
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="classes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"classes_read"})
     * @Assert\NotBlank(message="L'utilisateur (enseignement) est obligatoire")
     */
    private $user;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    /**
     * Permet de récupérer le moyen général d'un étudiant
     * 
     * @Groups({"students_read", "classes_read"})
     * @return float
     */
    public function getTotalScoreStudent(): float
    {           
        return array_reduce($this->students->toArray(), function($total, $student) {
            return $total + ($student->getTotalScore() / count($this->students->toArray()));
        }, 0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setClasse($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
            // set the owning side to null (unless already changed)
            if ($student->getClasse() === $this) {
                $student->setClasse(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
