<?php

namespace App\Entity;

use App\Repository\StudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 * @ApiResource(
 *  normalizationContext={"groups"={"students_read"}},
 *  denormalizationContext={"disable_type_enforcement"=true}
 * )
 */
class Student
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"students_read", "notes_read", "classes_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"students_read", "notes_read", "classes_read"})
     * @Assert\NotBlank(message="Le prénom de l'élève  est obligatoire")
     * @Assert\Length(min=3, minMessage="Le prénom de la matière doit faire entre 3 et 255 caractères", max=255, maxMessage="Le prénom de la matière doit faire entre 3 et 255 caractères")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"students_read", "notes_read", "classes_read"})
     * @Assert\NotBlank(message="Le nom de l'élève est obligatoire")
     * @Assert\Length(min=3, minMessage="Le nom de la matière doit faire entre 3 et 255 caractères", max=255, maxMessage="Le nom de la matière doit faire entre 3 et 255 caractères")
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"students_read"})
     * @Assert\NotBlank(message="La date de naissance de l'élève est obligatoire")
     * @Assert\Type(type="\DateTime", message="La date doit être au format YYYY-MM-DD")
     */
    private $birthDateAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"students_read"}) 
     * @Assert\NotBlank(message="L'adresse mail de l'élève est obligatoire")
     * @Assert\Email(message="Le format de l'adresse email doit être valide")
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=Classe::class, inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"students_read"})
     * @Assert\NotBlank(message="La classe dont l'élève appartient est obligatoire")
     */
    private $classe;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="student", orphanRemoval=true)
     * @Groups({"students_read"})
     */
    private $notes;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    /**
     * Permet de récupérer le total des coefficients
     * 
     * @Groups({"students_read"})
     * @return float
     */
    public function getTotalCoefficient(): float
    {
        return array_reduce($this->notes->toArray(), function($total, $note) {
            return $total + ($note->getSubject()->getCoefficient());
        }, 0);
    }


    /**
     * Permet de récupérer le moyen général d'un étudiant
     * 
     * @Groups({"students_read", "classes_read"})
     * @return float
     */
    public function getTotalScore(): float
    {   
        $total = 0;
        $to = array_reduce($this->notes->toArray(), function($total, $note) {
            return $total + (($note->getScore() * $note->getSubject()->getCoefficient()));
        }, 0);

        if(($this->getTotalCoefficient()) > 0) {
            $total = ($to / ($this->getTotalCoefficient())); 
        } 

        return $total;
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthDateAt(): ?\DateTimeInterface
    {
        return $this->birthDateAt;
    }

    public function setBirthDateAt($birthDateAt): self
    {
        $this->birthDateAt = $birthDateAt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setStudent($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getStudent() === $this) {
                $note->setStudent(null);
            }
        }

        return $this;
    }
}
