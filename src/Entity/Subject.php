<?php

namespace App\Entity;

use App\Repository\SubjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SubjectRepository::class)
 * @ApiResource(
 *  normalizationContext={"groups"={"subjects_read"}},
 *  denormalizationContext={"disable_type_enforcement"=true} 
 * )
 * @UniqueEntity("name", message="Un matière ayant cet nom existe déjà")
 */
class Subject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"subjects_read", "notes_read", "students_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom de la matière est obligatoire")
     * @Groups({"subjects_read", "notes_read", "students_read"})
     * @Assert\Length(min=3, minMessage="Le nom de la matière doit faire entre 3 et 255 caractères", max=255, maxMessage="Le nom de la matière doit faire entre 3 et 255 caractères")
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Groups({"subjects_read", "students_read"})
     * @Assert\NotBlank(message="Le coefficient est obligatoire")
     * @Assert\Type(type="numeric", message="Le coefficient doit être un numérique")
     */
    private $coefficient;

    /**
     * @ORM\Column(type="float")
     * @Groups({"subjects_read"})
     * @Assert\NotBlank(message="Le nombre d'heures de classe est obligatoire")
     * @Assert\Type(type="numeric", message="Le nombre d'heures doit être un numérique")
     */
    private $numberOfHours;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="subject", orphanRemoval=true)
     * 
     */
    private $notes;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCoefficient(): ?float
    {
        return $this->coefficient;
    }

    public function setCoefficient($coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getNumberOfHours(): ?float
    {
        return $this->numberOfHours;
    }

    public function setNumberOfHours($numberOfHours): self
    {
        $this->numberOfHours = $numberOfHours;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setSubject($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getSubject() === $this) {
                $note->setSubject(null);
            }
        }

        return $this;
    }
}
