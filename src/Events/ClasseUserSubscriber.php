<?php

namespace App\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Classe;
use Symfony\Component\Security\Core\Security;

class ClasseUserSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserForClasse', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForClasse(ViewEvent $event)
    {
        $Classe = $event->getControllerResult();

        $method = $event->getRequest()->getMethod();

        if($Classe instanceof Classe && $method === 'POST') {

            //choper l'utilisateur connecté
            $user = $this->security->getUser();

            // Assigner l'utilisateur au Classe qu'on est en train de créer
            $Classe->setUser($user);
        }
    }

}