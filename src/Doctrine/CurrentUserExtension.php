<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Classe;
use App\Entity\Student;
use App\Entity\Note;
use App\Entity\Subject;
use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface 
{
    private $security;
    private $auth;

    public function __construct(Security $security, AuthorizationCheckerInterface $checker)
    {
        $this->security = $security;
        $this->auth = $checker;
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass) 
    {
        // Obtenir l'utilisateur connecté
        $user = $this->security->getUser();
       // dd($user);
        
        // si on demande des Students ou Classes ou Note alors, agir sur la requete pour qu'elle tienne compte de user connecte
        if(($resourceClass === Classe::class || $resourceClass === Student::class || $resourceClass === Note::class ) && !$this->auth->isGranted('ROLE_ADMIN') && $user instanceof User) {
            
            $rootAlias = $queryBuilder->getRootAliases()[0];
            if($resourceClass === Classe::class) {
                $queryBuilder->andWhere("$rootAlias.user = :user");
            } elseif ($resourceClass === Student::class) {
                $queryBuilder->join("$rootAlias.classe", "c")
                    ->andWhere("c.user = :user");
            } elseif ($resourceClass === Note::class ) {
                $queryBuilder->join("$rootAlias.student", "s")
                    ->join("s.classe", "c")
                    ->join("$rootAlias.subject", "sb")
                    ->andWhere("c.user = :user");
            }

            $queryBuilder->setParameter("user", $user);

        }
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, ?string $operationName = null, array $context = [])
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }
}