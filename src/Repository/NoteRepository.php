<?php

namespace App\Repository;

use App\Entity\Note;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;


/**
 * @method Note|null find($id, $lockMode = null, $lockVersion = null)
 * @method Note|null findOneBy(array $criteria, array $orderBy = null)
 * @method Note[]    findAll()
 * @method Note[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NoteRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Note::class);
        $this->security = $security;
    }

    // /**
    //  * @return Note[] Returns an array of Note objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Note
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

 

    public function findAllNoteByUser() {
        $user = $this->security->getUser();

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT u.first_name, u.last_name, c.name, s.first_name, s.last_name, n.score, n.student_id, n.subject_id
            FROM App\Entity\User u
            Inner JOIN App\Entity\Classe c ON c.user_id = u.id
            Inner JOIN App\Entity\Student s ON s.classe_id = c.id
            Inner JOIN App\Entity\Note n ON n.student_id = s.id
            WHERE u.id = :user'
        )->setParameter('user', $user);
        
        dd($query->getResult());
    }
}
