<?php

namespace App\DataFixtures;

use App\Entity\Classe;
use App\Entity\Note;
use App\Entity\Student;
use App\Entity\Subject;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * L'encodeur de mots de passe
     *
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) 
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //
        $faker = Factory::create('fr_FR');

        // Data FIxtures de l'entité Subject
        for($su = 1; $su < 6; $su++) {
            
            $coefficients = array(1, 2, 3, 4, 5);
            $numberOfHours = array(2.5, 4, 6, 7.5, 8, 10);

            $subject = new Subject();
            $subject->setName('matière ' . $su)
                ->setCoefficient($coefficients[array_rand($coefficients, 1)])
                ->setNumberOfHours($numberOfHours[array_rand($numberOfHours, 1)]);
            // Associer une matière à une note
            $this->addReference('matière ' . $su, $subject);
            
            $manager->persist($subject);
            
        }

        // Data FIxtures de l'entité User
        for ($u=0; $u < 10; $u++) { 
            $user = new User();

            $hash = $this->encoder->encodePassword($user, "password");

            $user->setFirstName($faker->firstName())
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword($hash);
            
            $manager->persist($user);

            // Data FIxtures de l'entité Classe
            for ($c=1; $c < mt_rand(1, 4); $c++) { 
                $classe = new Classe();
                $classe->setName("classe $c")
                    ->setUser($user);
    
                $manager->persist($classe);
                
                // Data FIxtures de l'entité Student
                for ($s=0; $s < mt_rand(15, 30) ; $s++) { 
                    $student = new Student();
                    $student->setFirstName($faker->firstName())
                        ->setLastName($faker->lastName)
                        ->setEmail($faker->email)
                        ->setBirthDateAt($faker->dateTimeBetween('-6 months'))
                        ->setClasse($classe);
    
                    $manager->persist($student);

                    // Data FIxtures de l'entité Note
                    for ($n=0; $n < mt_rand(1, 10); $n++) { 
                        $note = new Note();
                        $scores = range(4, 20, 0.5);
                        $note->setScore($scores[array_rand($scores, 1)])
                            ->setStudent($student)
                            ->setSubject($this->getReference('matière ' . $faker->numberBetween(1, 5)));
                            
                        $manager->persist($note);
                    }

                }
            }

        }
        
        $manager->flush();
    }
    
}
