import React, { useState, useContext } from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Switch, Route, withRouter, Redirect, Router} from 'react-router-dom';
import AuthApi from './services/authApi';
import Navbar from './components/Navbar';
import PrivateRoute from './components/PrivateRoute';
import AuthContext from './contexts/AuthContext';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Classe from './pages/Classe';
import Classes from './pages/Classes';
import Student from './pages/Student';
import Students from './pages/Students';
import Subject from './pages/Subject';
import Subjects from './pages/Subjects';
import Note from './pages/Note';
import Notes from './pages/Notes';
import { ToastContainer, toast } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

import '../css/app.css';

AuthApi.setup();

const App = () => {
    //  par défaut on demande à notre AuthApi si on est connecté ou pas
    const [isAuthenticated, setIsAuthenticated] = useState(
        AuthApi.isAuthenticated()
    );

    const NavBarWithRouter = withRouter(Navbar);

    return (
        <AuthContext.Provider value={{
            isAuthenticated,
            setIsAuthenticated
        }}>
            <HashRouter>
                <NavBarWithRouter />
                    <main className="container pt-5">
                        <Switch>
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={Register} />
                            <PrivateRoute path="/classes/:id" component={Classe} />
                            <PrivateRoute path="/classes" component={Classes} />
                            <PrivateRoute path="/students/:id" component={Student} />
                            <PrivateRoute path="/students" component={Students} />
                            <PrivateRoute path="/subjects/:id" component={Subject} />
                            <PrivateRoute path="/subjects" component={Subjects} />
                            <PrivateRoute path="/notes/:id" component={Note} />
                            <PrivateRoute path="/notes" component={Notes} />
                            <Route path="/" component={Home} />
                        </Switch>            
                    </main>
            </HashRouter>
            <ToastContainer position={toast.POSITION.BOTTOM_LEFT}></ToastContainer>  
        </AuthContext.Provider>
    )
};

const rootElement = document.querySelector('#app');
ReactDOM.render(<App />, rootElement);

