const cache = {};

// Les données que je mets dans les caches (key et data)
function set(key, data) {
    cache[key] = {
        data,
        cacheAt: new Date().getTime()
    }
}

// Récupérer les données de cache en fonction de leur nom (key)
function get(key) {
    return new Promise((resolve) => {
        // 15 min
        resolve(cache[key]  && cache[key].cacheAt  + 15 * 60 * 1000 > new Date().getTime() ? cache[key].data : null);
    })
}

export default {
    set,
    get
}
