import axios from 'axios';
import { USERS_API } from "../config";

// Enregistrer un user dans la base
function register(user) {
    return axios.post(USERS_API, user);
}

export default {
    register
}