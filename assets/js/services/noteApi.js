import axios from 'axios';
import { NOTES_API } from "../config";

// Récupérer la liste des notes des élèves
function findAll() {
    return  axios.get(NOTES_API).then(response => response.data["hydra:member"]);
}

// Récupérer une note en fonction de son identifiant
function find(id) {
    return axios.get(NOTES_API + "/" + id ).then(response => response.data);
}

// Modifier une note en fonction de son identifiant
function update(id, note) {
    return axios.put(NOTES_API + "/" + id, {
        ...note,
        student: `/api/students/${note.student}`,
        subject: `/api/subjects/${note.subject}`
    });
}

// Créer une note pour un élève donné et une matière donnée
function create(note) {
    return axios.post(NOTES_API, {
        ...note,
        student: `/api/students/${note.student}`,
        subject: `/api/subjects/${note.subject}`
    });
}

// Supprimer une note en fonction de son identifiant
function deleteNote(id) {
    return axios.delete(NOTES_API + "/" + id);
}

export default {
    findAll,
    find,
    update,
    create,
    delete: deleteNote
}