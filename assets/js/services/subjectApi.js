import axios from 'axios';
import Cache from "./cache";
import { SUBJECTS_API } from "../config";

// Récupérer la liste des matières
async function findAll() {
    const cachedSubjects = await Cache.get("subjects");

    if (cachedSubjects) return cachedSubjects;

    return  axios.get(SUBJECTS_API).then(response => {
        const subjects = response.data["hydra:member"];
        Cache.set("subjects", subjects);
        return subjects;
    });
}

// Récupérer une matière en fonction de son identifiant
async function find(id) {
    const cachedSubject = await Cache.get("subjects." +id);

    if (cachedSubject) return cachedSubject;

    return axios
        .get(SUBJECTS_API + "/" + id )
        .then(response => {
            const subject = response.data;

            Cache.set("subjects." + id, subject);
            return response;
        })
}

// Modifier une matière en fonction de son identifiant
function update(id, subject) {
    return axios
        .put(SUBJECTS_API + "/" + id, subject)
        .then(async response => {
            const cachedSubjects = await Cache.get("subjects");
            const cachedSubject = await Cache.get("subjects." + id);

            if (cachedSubject) {
                Cache.set("subjects." + id, response.data)
            }

            if (cachedSubjects) {
                const index = cachedSubjects.findIndex(c => c.id === +id);
                cachedSubjects[index] = response.data;
            }

            return response;
        })
}

// Créer une matière
function create(subject) {
    return axios
        .post(SUBJECTS_API, subject)
        .then(async response => {
            const cachedSubjects = await Cache.get("subjects");

            if(cachedSubjects) {
                Cache.set("subjects", [...cachedSubjects, response.data]);
            }

            return response;
        });
    
}

// Supprimer une matière en fonction de son identifiant
function deleteSubject(id) {
    return axios
        .delete(SUBJECTS_API + "/" + id)
        .then(async response => {
            const cachedsubjects = await Cache.get("subjects");

            if(cachedsubjects) {
                Cache.set("subjects", cachedsubjects.filter(c => c.id !== id));
            }

            return response;
        });
}

export default {
    findAll,
    find,
    update,
    create,
    delete: deleteSubject
}