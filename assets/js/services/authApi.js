import axios from 'axios';
import JwtDecode from 'jwt-decode';
import { LOGIN_API } from "../config";


function authenticate(credentials) {
    return axios
        .post(LOGIN_API, credentials)
        .then(response => response.data.token)
        .then(token => {
            // je stocke le token dans mon localStorage
            window.localStorage.setItem("authToken", token);
            // on previent axios qu'on a maintenant un header par défaut sur toutes nos futures rêquetes HTTP
            axios.defaults.headers["Authorization"] = "Bearer " + token;
        })
}

function logout() {
    window.localStorage.removeItem("authToken");
    delete axios.defaults.headers["Authorization"];
}

function setup() {
    // voir si on a un toke
    const token = window.localStorage.getItem("authToken");
    // si le token est encore valide
    if(token) {
        const { exp: expiration } = JwtDecode(token);
        if(expiration * 1000 > new Date().getTime()) {
            axios.defaults.headers["Authorization"] = "Bearer " + token;
        } else {
            logout();
        }
    } else {
        logout();
    }
}

function isAuthenticated() {
    const token = window.localStorage.getItem("authToken");
    // si le token est encore valide
    if(token) {
        const { exp: expiration } = JwtDecode(token);
        if(expiration * 1000 > new Date().getTime()) {
            return true
        } else {
            return false;
        }
    } else {
        return false;
    }
}


export default {
    authenticate,
    logout,
    setup,
    isAuthenticated
}