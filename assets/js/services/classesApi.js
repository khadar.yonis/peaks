import axios from 'axios';
import Cache from "./cache";
import { CLASSES_API } from "../config";

// Récupérer la liste de classes
async function findAll() {
    const cachedClasses = await Cache.get("classes");

    if (cachedClasses) return cachedClasses;

    return  axios.get(CLASSES_API).then(response => {
        const classes = response.data["hydra:member"];
        Cache.set("classes", classes);
        return classes;
    });
}

// Récupérer une classe en fonction de son identifiant
async function find(id) {
    const cachedClasse = await Cache.get("classes." +id);

    if (cachedClasse) return cachedClasse;

    return axios
        .get(CLASSES_API + "/" + id)
        .then(response => {
            const classe = response.data;
            Cache.set("classes." + id, classe);
            return response;
        })
}

// Modifier une classe en fonction de son identifiant
function update(id, classe) {
    return axios
        .put(CLASSES_API + "/" + id , classe)
        .then(async response => {
            const cachedClasses = await Cache.get("classes");
            const cachedClasse = await Cache.get("classes." + id);

            if (cachedClasse) {
                Cache.set("classes." + id, response.data)
            }

            if (cachedClasses) {
                const index = cachedClasses.findIndex(c => c.id === +id);
                cachedClasses[index] = response.data;
            }

            return response;
        })
}

// Créer une classe
function create(classe) {
    return axios
        .post(CLASSES_API, classe)
        .then(async response => {
            const cachedClasses = await Cache.get("classes");

            if(cachedClasses) {
                Cache.set("classes", [...cachedClasses, response.data]);
            }

            return response;
        });
    
}

// Supprimer une classe en fonction de son identifiant
function deleteClasse(id) {
    return axios
        .delete(CLASSES_API + "/" + id)
        .then(async response => {
            const cachedClasses = await Cache.get("classes");

            if(cachedClasses) {
                Cache.set("classes", cachedClasses.filter(c => c.id !== id));
            }

            return response;
        });
}

export default {
    findAll,
    find,
    update,
    create,
    delete: deleteClasse
}