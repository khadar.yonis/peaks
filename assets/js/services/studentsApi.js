import axios from 'axios';
import { STUDENTS_API } from "../config";

// Récupérer la liste des élèves
function findAll() {
    return  axios.get(STUDENTS_API).then(response => response.data["hydra:member"]);
}

// Récupérer un élève en fonction de son identifiant
function find(id) {
    return axios.get(STUDENTS_API + "/" + id).then(response => response.data);
}

// Modifier un élève en fonction de son identifiant
function update(id, student) {
    return axios.put((STUDENTS_API + "/" + id), {
        ...student,
        classe: `/api/classes/${student.classe}`
    });
}

// Créer un élève
function create(student) {
    return axios.post(STUDENTS_API, {
        ...student,
        classe: `/api/classes/${student.classe}`
    });
}


// Supprimer un élève en fonction de son identifiant
function deleteStudent(id) {
    return axios.delete(STUDENTS_API + "/" + id);
}

export default {
    findAll,
    find,
    update,
    create,
    delete: deleteStudent
}