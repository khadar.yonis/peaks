import React, { useEffect, useState } from 'react';
import ClassesApi from "../services/classesApi";
import Pagination from '../components/Pagination';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../components/loaders/TableLoader';

const Classes = propos => {
    
    const itemsPerPage = 10;
    const [classes, setClasses] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true);

    // permet de récupérer les classes
    const fetchClasses = async () => {
        try {
            const data = await ClassesApi.findAll();
            setClasses(data);
            setLoading(false);
        } catch (error) {
            toast.error("Erreur lors du chargement des classes !");
        }
    }

    // au chargement du composant, on va chercher les classes
    useEffect(() => {
        fetchClasses();
    }, []);

    // gestion du changement de page
    const handlePageChange = page => setCurrentPage(page);
    
    // gestion de la recherche
    const handleSearch = ({ currentTarget }) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    // filtrage des classes en fonction de la recherche
    const filteredClasses = classes.filter( 
        c => 
        c.name.toLowerCase().includes(search.toLowerCase())
    );
        
    // pagination des classes
    const paginatedClasses = Pagination.getData(filteredClasses, currentPage, itemsPerPage);
    

    // gestion de la suppression d'une classe
    const handleDelete =  async id => {
        const originalClasses = [...classes];
        setClasses(classes.filter(classe => classe.id !== id));

        try {
            await ClassesApi.delete(id);
            toast.success("La classe a bien été supprimée");
        } catch (error) {
            toast.error("Une erreur est survenue");
            setClasses(originalClasses);
        }
    };

    return <>
        <div className="mb-3 d-flex justify-content-between align-items-center">
            <h1>Liste des classes</h1>
            <Link to="/classes/new" className="btn btn-primary">Ajouter une classe</Link>
        </div>

        <div className="form-group">
            <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher..."/>
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Id.</th>
                    <th className="text-center">Prof (user)</th>
                    <th className="text-center">Nom de la classe</th>
                    <th className="text-center">Moyen général</th>
                    <th></th>
                </tr>
            </thead>

            { !loading && (
                <tbody>
                    {paginatedClasses.map(classe => <tr key={classe.id}>
                        <td>{classe.id}</td>
                        <td className="text-center">{classe.user.firstName} {classe.user.lastName}</td>
                        <td className="text-center">{classe.name}</td>
                        <td className="text-center">{classe.totalScoreStudent.toFixed(2)}</td>
                        <td>
                            <Link
                            to={`/classes/${classe.id}`}
                                className="btn btn-sm btn-primary mr-2">
                                Modifier
                            </Link>
                            <button
                                onClick={() => {if(window.confirm('êtes-vous sûr de bien vouloire supprimer?')){handleDelete(classe.id)};}} 
                                className="btn btn-sm btn-danger">
                                Supprimer
                            </button>
                        </td>
                    </tr>)}                   
                </tbody>
            )}
        </table>
        
        {loading && <TableLoader /> }
        
        {itemsPerPage < filteredClasses.length && 
            <Pagination 
                currentPage={currentPage} 
                itemsPerPage={itemsPerPage} 
                length={filteredClasses.length} 
                onPageChanged={handlePageChange}
            />
        }
    </>;
}
 
export default Classes;