import React, { useEffect, useState } from 'react';
import SubjectApi from '../services/subjectApi';
import Pagination from '../components/Pagination';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../components/loaders/TableLoader';

const Subjects = propos => {

    const itemsPerPage = 4;
    const [subjects, setSubjects] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true);

    // permet de récupérer les matières
    const fetchSubjects = async () => {
        try {
            const data = await SubjectApi.findAll();
            setSubjects(data);
            setLoading(false);
        } catch (error) {
            toast.error("Erreur lors du chargement des matières !");
        }
    }

    // au chargement du composant, on va chercher les matières
    useEffect(() => {
        fetchSubjects();
    }, []);

    // gestion du changement de page
    const handlePageChange = page => setCurrentPage(page);
    
    // gestion de la recherche
    const handleSearch = ({ currentTarget }) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    // filtrage des matières en fonction de la recherche
    const filteredSubjects = subjects.filter(s => 
        s.name.toLowerCase().includes(search.toLowerCase()) ||
        s.coefficient.toLocaleString().includes(search.toLowerCase()) ||
        s.numberOfHours.toLocaleString().includes(search.toLowerCase())
    );
        
    // pagination des matières
    const paginatedSubjects = Pagination.getData(filteredSubjects, currentPage, itemsPerPage);
    

    // gestion de la suppression d'une matière
    const handleDelete =  async id => {
        const originalSubjects = [...subjects];
        setSubjects(subjects.filter(subject => subject.id !== id));

        try {
            await SubjectApi.delete(id);
            toast.success("La matière a bien été supprimée");
        } catch (error) {
            toast.error("Une erreur est survenue");
            setClasses(originalSubjects);
        }
    };


    return <>
        <div className="mb-3 d-flex justify-content-between align-items-center">
            <h1>Liste des matières</h1>
            <Link to="/subjects/new" className="btn btn-primary">Ajouter une matière</Link>
        </div>

        <div className="form-group">
            <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher..."/>
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Id.</th>
                    <th>Nom de la matière</th>
                    <th className="text-center">Coefficient</th>
                    <th className="text-center">Nombre d'heures <br /> (par semaine)</th>                  
                    <th></th>
                </tr>
            </thead>

            { !loading && (
                <tbody>
                    {paginatedSubjects.map(subject => <tr key={subject.id}>
                        <td>{subject.id}</td>
                        <td>{subject.name}</td>
                        <td className="text-center">{subject.coefficient}</td>
                        <td className="text-center">{subject.numberOfHours}</td>
                        <td>
                            <Link
                                to={`/subjects/${subject.id}`}
                                className="btn btn-sm btn-primary mr-2">
                                Modifier
                            </Link>
                            <button 
                                onClick={() => {if(window.confirm('êtes-vous sûr de bien vouloire supprimer?')){handleDelete(subject.id)};}} 
                                className="btn btn-sm btn-danger">
                            Supprimer</button>
                        </td>
                    </tr>)}                   
                </tbody>
            )}
        </table>

        {loading && <TableLoader /> }

        {itemsPerPage < filteredSubjects.length && 
            <Pagination 
                currentPage={currentPage} 
                itemsPerPage={itemsPerPage} 
                length={filteredSubjects.length} 
                onPageChanged={handlePageChange}
            />
        }

    </>;
}
 
export default Subjects;