import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Field from '../components/forms/Field';
import Select from '../components/forms/Select';
import NoteApi from '../services/noteApi';
import StudentsApi from '../services/studentsApi';
import SubjectsApi from '../services/subjectApi';

const Note = ({ match, history }) => {

    const { id  = "new" } = match.params;

    const [editing, setEditing] = useState(false);
    const [students, setStudents] = useState([]);
    const [subjects, setSubjects] = useState([]);

    const [note, setNote] = useState({
        score: "",
        student: "",
        subject: ""
    });

    const [errors, setErrors] = useState({
        score: "",
        student: "",
        subject: ""
    });

    const fetchStudents = async () => {
        try {
            const data = await StudentsApi.findAll();
            setStudents(data);
            if(!note.student)  setNote({ ...note, student: data[0].id });   
                   
           
        } catch (error) {
            history.replace('/notes');
        }
    }

    const fetchSubjects = async () => {
        try {
            const data = await SubjectsApi.findAll();
            setSubjects(data);
            if(!note.subject)  setNote({ ...note, subject: data[0].id });
        } catch (error) {
            history.replace('/notes');
        }
    }

    // Récupération de la note en fonction de l'élève et l'utilisateur connecté
    const fetchNote = async id => {
        try {
            const { score, student, subject } = await NoteApi.find(id);
            setNote({ score, student: student.id, subject: subject.id });
        } catch (error) {
            history.replace("/notes");
        }
    }
    
    useEffect(() => {
        fetchStudents();
        fetchSubjects();
    }, []); 

    useEffect(() => {
        if(id !== "new") {
            setEditing(true);
            fetchNote(id);
        }
    }, [id]);

    const handleChange = ({currentTarget}) => {
        const { name, value } = currentTarget;
        setNote({ ...note, [name]: value });
    };

    const handleSubmit = async event => {
        event.preventDefault();
        try {
            if(editing) {
                await NoteApi.update(id, note);
            } else {
                await NoteApi.create(note);                
            }
            
            setErrors([]);
            history.replace("/notes");

        } catch ({ response }) {
            console.log(response);
            const { violations } = response.data;
            if(violations) {
                const apiErrors = {};
                violations.forEach(violation => {
                    apiErrors[violation.propertyPath] = violation.message
                });
                setErrors(apiErrors);
            }
        }
    }; 

    return ( 
        <>
            {(!editing && <h1>Ajout d'une note</h1>) || (
                <h1>Modification d'une note</h1>
            )}

            <form onSubmit={handleSubmit}>
                <Field name="score" label="Note" placeholder="la note" value={note.score} onChange={handleChange} error={errors.score}/>
                <Select name="student" label="Etudiant" value={note.score} onChange={handleChange}>
                    {students.map(student => 
                        <option key={student.id} value={student.id}>{student.lastName} {student.firstName}</option>)}
                </Select>
                <Select name="subject" label="Matière" value={note.subject} onChange={handleChange}>
                    {subjects.map(subject => 
                        <option key={subject.id} value={subject.id}>{subject.name}</option>)}
                </Select>

                <div className="form-group">
                    <button type="submit" className="btn btn-sucess">
                        Enregistrer
                    </button>
                    <Link to="/notes" className="btn btn-link">
                        Retour à la liste
                    </Link>
                </div>
            </form>
        </>
     );
}
 
export default Note;