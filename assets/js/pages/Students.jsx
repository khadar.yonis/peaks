import React, { useEffect, useState } from 'react';
import moment from 'moment';
import Pagination from '../components/Pagination';
import StudentsApi from '../services/studentsApi';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../components/loaders/TableLoader';

const Students = propos => {
    const itemsPerPage = 10;
    const [students, setStudents] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true);


    const formatDate = str => moment(str).format('DD/MM/YYYY');

    // permet de récupérer les élèves
    const fetchStudents = async () => {
        try {
            const data = await StudentsApi.findAll();
            setStudents(data);
            setLoading(false);
        } catch (error) {
            toast.error("Erreur lors du chargement des élèves !");
        }
    }

    // au chargement du composant, on va chercher les élèves
    useEffect(() => {
        fetchStudents();
    }, []);

    // gestion du changement de page
    const handlePageChange = page => setCurrentPage(page);
    
    // gestion de la recherche
    const handleSearch = ({ currentTarget }) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    // filtrage des élèves en fonction de la recherche
    const filteredStudents = students.filter(s => 
        s.firstName.toLowerCase().includes(search.toLowerCase()) ||
        s.lastName.toLowerCase().includes(search.toLowerCase()) ||
        s.email.toLowerCase().includes(search.toLowerCase()) ||
        s.classe.name.toLowerCase().includes(search.toLowerCase())
    );
        
    // pagination des élèves
    const paginatedStudents = Pagination.getData(filteredStudents, currentPage, itemsPerPage);
    

    // gestion de la suppression d'un élève
    const handleDelete =  async id => {
        const originalStudents = [...students];
        setStudents(students.filter(student => student.id !== id));

        try {
            await StudentsApi.delete(id);
            toast.success("L'élève a bien été supprimé");
        } catch (error) {
            toast.error("Une erreur est survenue");
            setStudents(originalStudents);
        }
    };    


    return <>
        <div className="mb-3 d-flex justify-content-between align-items-center">
            <h1>Liste des élèves</h1>
            <Link to="/students/new" className="btn btn-primary">Ajouter un élève</Link>
        </div>

        <div className="form-group">
            <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher..."/>
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Id.</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Date de naissance</th>
                    <th className="text-center">Classe</th>
                    <th className="text-center">Email</th>  
                    <th className="text-center">Moyen Général</th>                  
                    <th></th>
                </tr>
            </thead>

            { !loading && (
                <tbody>
                    {paginatedStudents.map(student => <tr key={student.id}>
                        <td>{student.id}</td>
                        <td>{student.firstName}</td>
                        <td>{student.lastName}</td>
                        <td>{formatDate(student.birthDateAt)}</td>                    
                        <td className="text-center">
                            <span className="badge badge-primary">{student.classe.name}</span>
                        </td>
                        <td className="text-center">{student.email}</td>
                        <td>{student.totalScore.toFixed(2)}</td>
                        <td>
                            <Link
                            to={`/students/${student.id}`}
                                className="btn btn-sm btn-primary mr-2">
                                Modifier
                            </Link>
                            <button
                                onClick={() => {if(window.confirm('êtes-vous sûr de bien vouloire supprimer?')){handleDelete(student.id)};}} 
                                className="btn btn-sm btn-danger">
                            Supprimer</button>
                        </td>
                    </tr>)}                   
                </tbody>
            )}
        </table>

        {loading && <TableLoader /> }

        {itemsPerPage < filteredStudents.length && 
            <Pagination 
                currentPage={currentPage} 
                itemsPerPage={itemsPerPage} 
                length={filteredStudents.length} 
                onPageChanged={handlePageChange}
            />
        }
    </>;
}
 
export default Students;