import React,{useState, useContext} from 'react';
import AuthApi from '../services/authApi';
import AuthContext from '../contexts/AuthContext';
import Field from '../components/forms/Field';
import { toast } from 'react-toastify';

const LoginPage = ({history}) => {
    const { setIsAuthenticated } = useContext(AuthContext);
    const [credentials, setCredentials] = useState({
        username: "",
        password: ""
    });

    const [error, setError] = useState("");
    // gestion des champs
    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget;

        setCredentials({...credentials, [name]: value});
    };
    // gestion du submit
    const handleSubmit = async event => {
        event.preventDefault();

        try {
            await AuthApi.authenticate(credentials);
            setError("");
            setIsAuthenticated(true);
            toast.success("Vous êtes bien connecté");
            history.replace("/classes");
        } catch (error) {
            setError("L'email ou le mot de passe est incorrect");
            toast.error("Une erreur est survenue");
        }

    }

    return ( 
        <>
            <h1>Connexion à l'application</h1>

            <form onSubmit={handleSubmit}>

                <Field 
                    label="Email" 
                    name="username" 
                    value={credentials.username} 
                    onChange={handleChange}
                    placeholder="Adresse email de connexion" 
                    error={error} 
                />
                
                <Field 
                    label="Mot de passe" 
                    name="password" 
                    value={credentials.password} 
                    onChange={handleChange}
                    type="password"
                    error="" 
                />

                <div className="form-group">
                    <button className="btn btn-sucess" type="submit">Connecter</button>
                </div>
            </form>
        </>
    );
}
 
export default LoginPage;