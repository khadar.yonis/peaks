import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Field from '../components/forms/Field';
import StudentsApi from "../services/studentsApi";
import Select from '../components/forms/Select';
import ClassesApi from '../services/classesApi';
import { toast } from 'react-toastify';


const Student = ({ match, history }) => {

    const { id  = "new" } = match.params;

    const [editing, setEditing] = useState(false);
    const [classes, setClasses] = useState([]);

    const [student, setStudent] = useState({
        firstName: "",
        lastName: "",
        birthDateAt: "",
        email: "",
        classe: ""
    });

    const [errors, setErrors] = useState({
        firstName: "",
        lastName: "",
        birthDateAt: "",
        email: "",
        classe: ""
    });

    const fetchClasses = async () => {
        try {
            const data = await ClassesApi.findAll();
            setClasses(data);
            if(!student.classe) setStudent({ ...student, classe: data[0].id });
           
        } catch (error) {
            history.replace('/students');
        }
    }

    // Récupération de l'éléve en fonction de l'utilisateur connecté
    const fetchStudent = async id => {
        try {
            const { firstName, lastName, birthDateAt, email, classe } = await StudentsApi.find(id);
            console.log(firstName, lastName, birthDateAt, email, classe);
            setStudent({ firstName, lastName, birthDateAt, email, classe: classe.id });
        } catch (error) {
            history.replace("/students");
        }
    }
    
    useEffect(() => {
        fetchClasses();
    }, []);

    useEffect(() => {
        if(id !== "new") {
            setEditing(true);
            fetchStudent(id); 
        }
    }, [id]);

    // gestion de la changement
    const handleChange = ({currentTarget}) => {
        const { name, value } = currentTarget;
        setStudent({ ...student, [name]: value });
    };

    // gestion de la soumission
    const handleSubmit = async event => {

        event.preventDefault();

        const apiErrors = {};

        console.log(student.classe);

        if(student.birthDateAt === "") {
            apiErrors.birthDateAt = "La date de naissance est obligatoire";
            setErrors(apiErrors);
            toast.error("Des erreurs dans votre formulaire");
            return;
        }

        try {
            if(editing) {
                await StudentsApi.update(id, student);
            } else {

                await StudentsApi.create(student);
            }
            
            setErrors([]);
            history.replace("/students");

        } catch ({ response }) {
            const { violations } = response.data;
            if(violations) {
                
                violations.forEach(violation => {
                    apiErrors[violation.propertyPath] = violation.message
                });
                setErrors(apiErrors);
            }
        }
    }; 


    return ( 
        <>
            {(!editing && <h1>Ajouter d'un élève</h1>) || (
                <h1>Modification d'un élève</h1>
            )}

            <form onSubmit={handleSubmit}>
                <Field name="firstName" label="Prénom" placeholder="Prénom de l'élève" value={student.firstName} onChange={handleChange} error={errors.firstName}/>
                <Field name="lastName" label="Nom" placeholder="Nom de l'élève" value={student.lastName} onChange={handleChange} error={errors.lastName}/>
                <Field name="birthDateAt" label="Date de naissance" placeholder="format JJ/MM/AAAA" value={student.birthDateAt}  onChange={handleChange} error={errors.birthDateAt}/>
                <Field name="email" label="Email" placeholder="Adresse email de l'élève" value={student.email}  onChange={handleChange} error={errors.email}/>
                <Select name="classe" label="Classe" value={student.classe} error={errors.classe} onChange={handleChange}>
                    {classes.map(classe => 
                        <option key={classe.id} value={classe.id}>{classe.name}</option>)}
                </Select>

                <div className="form-group">
                    <button type="submit" className="btn btn-sucess">
                        Enregistrer
                    </button>
                    <Link to="/students" className="btn btn-link">
                        Retour à la liste
                    </Link>
                </div>
            </form>
        </>
     );
}
 
export default Student;