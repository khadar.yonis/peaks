import React, { useEffect, useState } from 'react';
import Pagination from '../components/Pagination';
import NotesApi from '../services/noteApi';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import TableLoader from '../components/loaders/TableLoader';

const Notes = propos => {

    const itemsPerPage = 10;
    const [notes, setNotes] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true);

    // permet de récupérer les notes
    const fetchNotes = async () => {
        try {
            const data = await NotesApi.findAll();
            setNotes(data);
            setLoading(false);
        } catch (error) {
            toast.error("Erreur lors du chargement des matières !");
        }
    }

    // au chargement du composant, on va chercher les notes
    useEffect(() => {
        fetchNotes();
    }, []);

    // gestion du changement de page
    const handlePageChange = page => setCurrentPage(page);
    
    // gestion de la recherche
    const handleSearch = ({ currentTarget }) => {
        setSearch(currentTarget.value);
        setCurrentPage(1);
    };

    // filtrage des notes en fonction de la recherche
    const filteredNotes = notes.filter(n => 
        n.score.toFixed(2).includes(search.toLowerCase()) ||
        n.subject.name.toLowerCase().includes(search.toLowerCase()) ||
        n.student.firstName.toLowerCase().includes(search.toLowerCase()) ||
        n.student.lastName.toLowerCase().includes(search.toLowerCase())
    );
        
    // pagination des notes
    const paginatedNotes = Pagination.getData(filteredNotes, currentPage, itemsPerPage);
    

    // gestion de la suppression d'une note
    const handleDelete =  async id => {
        const originalNotes = [...notes];
        setNotes(notes.filter(student => student.id !== id));

        try {
            await NotesApi.delete(id);
            toast.success("La note a bien été supprimée");
        } catch (error) {
            toast.error("Une erreur est survenue");
            setNotes(originalNotes);
        }
    };    


    return <>
        <div className="mb-3 d-flex justify-content-between align-items-center">
            <h1>Liste des notes</h1>
            <Link to="/notes/new" className="btn btn-primary">Ajouter une note</Link>
        </div>

        <div className="form-group">
            <input type="text" onChange={handleSearch} value={search} className="form-control" placeholder="Rechercher..."/>
        </div>

        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Id.</th>
                    <th className="text-center">Etudiant</th>
                    <th className="text-center">Score</th>
                    <th className="text-center">Matière</th>                  
                    <th></th>
                </tr>
            </thead>

            { !loading && (
                <tbody>
                    {paginatedNotes.map(note => <tr key={note.id}>
                        <td>{note.id}</td>
                        <td className="text-center">{note.student.firstName} {note.student.lastName} ({note.student.id})</td>
                        <td className="text-center">{note.score}</td>
                        <td className="text-center">{note.subject.name}</td>
                        <td>
                            <Link
                            to={`/notes/${note.id}`}
                                className="btn btn-sm btn-primary mr-2">
                                Modifier
                            </Link>
                            <button 
                                onClick={() => {if(window.confirm('êtes-vous sûr de bien vouloire supprimer?')){handleDelete(note.id)};}} 
                                className="btn btn-sm btn-danger">Supprimer</button>
                        </td>
                    </tr>)}                   
                </tbody>
            )}
        </table>

        {loading && <TableLoader /> }

        {itemsPerPage < filteredNotes.length && 
            <Pagination 
                currentPage={currentPage} 
                itemsPerPage={itemsPerPage} 
                length={filteredNotes.length} 
                onPageChanged={handlePageChange}
            />
        }
    </>;
}
 
export default Notes;