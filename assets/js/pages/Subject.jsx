import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Field from '../components/forms/Field';
import SubjectApi from "../services/subjectApi";

const Subject = ({ match, history }) => {

    const { id  = "new" } = match.params;

    const [subject, setSubject] = useState({
        name: "",
        coefficient: "",
        numberOfHours: ""
    });

    const [errors, setErrors] = useState({
        name: "",
        coefficient: "",
        numberOfHours: ""
    });

    const [editing, setEditing] = useState(false);

    // Récupération de la matière en fonction de l'utilisateur connecté
    const fetchSubject = async id => {
        try {
            const { name, coefficient, numberOfHours } = await SubjectApi.find(id);
            setSubject({ name, coefficient, numberOfHours });
        } catch (error) {
            history.replace("/subjects");
        }
    }
        
    useEffect(() => {
        if(id !== "new") {
            setEditing(true);
            fetchSubject(id);
        }
    }, [id]);

    const handleChange = ({currentTarget}) => {
        const { name, value } = currentTarget;
        setSubject({ ...subject, [name]: value });
    };

    const handleSubmit = async event => {

        event.preventDefault();

        try {
            if(editing) {
                await SubjectApi.update(id, subject);
            } else {
                await SubjectApi.create(subject);
            }
            
            setErrors([]);
            history.replace("/subjects");
        } catch ({ response }) {
            const { violations } = response.data;
            if(violations) {
                const apiErrors = {};
                violations.forEach(violation => {
                    apiErrors[violation.propertyPath] = violation.message
                });
                setErrors(apiErrors);
            }
        }
    }; 

    return ( 
        <>
            {(!editing && <h1>Création d'une matière</h1>) || (
                <h1>Modification d'une matière</h1>
            )}

            <form onSubmit={handleSubmit}>
                <Field name="name" label="Nom de la matière" placeholder="Nom de la matière" value={subject.name} onChange={handleChange} error={errors.name}/>
                <Field name="coefficient" label="Coefficient" placeholder="Coefficient" value={subject.coefficient} onChange={handleChange} error={errors.coefficient}/>
                <Field name="numberOfHours" label="Nombre d'heures" placeholder="Nombre d'heures" value={subject.numberOfHours}  onChange={handleChange} error={errors.numberOfHours}/>

                <div className="form-group">
                    <button type="submit" className="btn btn-sucess">
                        Enregistrer
                    </button>
                    <Link to="/subjects" className="btn btn-link">
                        Retour à la liste
                    </Link>
                </div>
            </form>
        </>
     );
}
 
export default Subject;