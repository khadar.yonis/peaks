import React, { useState } from 'react';
import Field from '../components/forms/Field';
import { Link } from 'react-router-dom';
import UsersApi from '../services/usersApi';
import { toast } from 'react-toastify';


const Register = ({ history }) => {

    const [user, setUser] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        passwordConfirm: ""
    });

    const [errors, setErros] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        passwordConfirm: ""
    });

    const handleChange = ({ currentTarget }) => {
        const { name, value } = currentTarget;
        setUser({ ...user, [name]: value });
    };

    // Gestion de la soumission
    const handleSubmit = async event => {
        event.preventDefault();

        const apiErrors = {};

        if(user.password !== user.passwordConfirm) {
            apiErrors.passwordConfirm = "Votre confirmation de mot de passe n'est pas conforme avec le mot de passe original";
            setErros(apiErrors);
            toast.error("Des erreurs dans votre formulaire");
            return;
        }

        try {
            await UsersApi.register(user);          
            setErros([]);
            // flash succes
            toast.success("Vous êtes désormais inscrit, vous pouvez vous connecter!");
            history.replace('/login');
        } catch (error) {
            const {violations} = error.response.data;
            if(violations) {                
                violations.forEach(violation => {
                    apiErrors[violation.propertyPath] = violation.message
                });
                setErros(apiErrors);
            }
        }
        
        toast.error("Des erreurs dans votre formulaire");
    };

    return ( 
        <>
            <h1>Formulaire d'inscription</h1>
            <form onSubmit={handleSubmit}>
                    <Field name="firstName" label="Prénom" placeholder="votre prénom" error={errors.firstName} value={user.firstName} onChange={handleChange}></Field>
                    <Field name="lastName" label="Nom" placeholder="votre nom de famille" error={errors.LastName} value={user.LastName} onChange={handleChange}></Field>
                    <Field name="email" label="Email" placeholder="votre email" error={errors.email} value={user.email} onChange={handleChange} type="email"></Field>
                    <Field name="password" label="Mot de passe" placeholder="votre mot de passe" error={errors.password} value={user.password} onChange={handleChange} type="password"></Field>
                    <Field name="passwordConfirm" label="Confirmation de mot de passe" placeholder="confirmez votre mot de passe" error={errors.passwordConfirm} value={user.passwordConfirm} onChange={handleChange} type="password"></Field>

                <div className="form-group">
                    <button type="submit" className="btn btn-sucess">
                        Confirmation
                    </button>
                    <Link to="/login" className="btn btn-link">
                        J'ai déjà un compte
                    </Link>
                </div>
            </form>
        </>
    );
}
 
export default Register;