import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Field from '../components/forms/Field';
import ClassesApi from "../services/classesApi";

const Classe= ({ match, history }) => {

    const { id  = "new" } = match.params;

    const [classe, setClasse] = useState({
        name: ""
    });

    const [errors, setErrors] = useState({
        name: ""
    })

    const [editing, setEditing] = useState(false);

    // Récupération de la classe en fonction de l'utilisateur connecté
    const fetchClasse = async id => {
        try {
            const { name } = await ClassesApi.find(id);
            setClasse({ name });
        } catch (error) {
            history.replace("/classes");
        }
    }       

    useEffect(() => {
        if(id !== "new") {
            setEditing(true);
            fetchClasse(id);
        }
    }, [id]);

    // gestion de la changement 
    const handleChange = ({currentTarget}) => {
        const { name, value } = currentTarget;
        setClasse({ ...classe, [name]: value });
    };

    // gestion de la soumission
    const handleSubmit = async event => {
        event.preventDefault();

        try {
            if(editing) {
                await ClassesApi.update(id, classe);               
            } else {
                await ClassesApi.create(classe);               
            }

            setErrors([]);
            history.replace("/classes");
            
        } catch ({ response }) {
            const { violations } = response.data;
            if(violations) {
                const apiErrors = {};
                violations.forEach(violation => {
                    apiErrors[violation.propertyPath] = violation.message
                });
                setErrors(apiErrors);
            }
        }
    }; 


    return (
        <>
            {(!editing && <h1>Ajout d'une classe</h1>) || (
                <h1>Modification d'une classe</h1>
            )}

            <form onSubmit={handleSubmit}>
                <Field name="name" label="Nom de la classe" placeholder="Nom de la classe" value={classe.name} onChange={handleChange} error={errors.name}/>

                <div className="form-group">
                    <button type="submit" className="btn btn-sucess">
                        Enregistrer
                    </button>
                    <Link to="/classes" className="btn btn-link">
                        Retour à la liste des classes
                    </Link>
                </div>
            </form>
        </>
    );
}
 
export default Classe;