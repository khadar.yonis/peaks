import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import AuthContext from '../contexts/AuthContext';
import AuthApi from '../services/authApi';
import { toast } from "react-toastify";

const Navbar = ({history}) => {

    const { isAuthenticated, setIsAuthenticated} = useContext(AuthContext);

    const handleLogout = () => {
        AuthApi.logout();
        setIsAuthenticated(false);
        toast.info("Vous êtes désormais déconnecté");
        history.push("/login");
    };

    return ( 
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <NavLink className="navbar-brand" to="/">Ecole</NavLink>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
    
        <div className="collapse navbar-collapse" id="navbarColor03">
        <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <NavLink className="nav-link" to="/classes">Classes</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/students">Élèves</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/subjects">Matières</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/notes">Notes</NavLink>
            </li>
        </ul>
        <ul className="navbar-nav ml-auto">
            {(!isAuthenticated && (
                <>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/register">Inscription</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="btn btn-success" to="/login">Connexion</NavLink>
                </li>
                </>
            )) || (
                <li className="nav-item">
                    <button onClick={handleLogout} className="btn btn-danger" href="#">Déconnexion</button>
                </li>   
            )}
        </ul>
        </div>
    </nav> 
    );
}
 
export default Navbar;