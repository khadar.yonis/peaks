// Attention pour moi mon serveur local est https://localhost:8000 remplacer par le votre
export const API_URL = "https://localhost:8000/api/";

export const CLASSES_API = API_URL + "classes";
export const STUDENTS_API = API_URL + "students";
export const SUBJECTS_API = API_URL + "subjects";
export const NOTES_API = API_URL + "notes";
export const USERS_API = API_URL + "users";
export const LOGIN_API = API_URL + "login_check";

